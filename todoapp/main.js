$(function(){
    
    //hozzáad gomb lenyomása
    $("#add").on("click",function(event){
        
        var text = $("#text").val();

        if(text==""){
            event.preventDefault();
        }
        else{
            $("ol").append("<li>" + text + "<i class='fa fa-trash ml-3'></i> </li>");
            $("#text").val("");
        }
        
    })

    //Feladat hozzáadása a számozatlan listához ENTER gomb lenyomásával
    //keycode.info-n megnézve az enter kódja
    $("#text").on("keyup",function(event){
        if(event.keyCode=="13"){
            $("#add").click();
        }
    })

    //kuka ikonra duplán kattintva az adott feladatot törli ki
    $(document).on("dblclick","i",function(){
        $(this).parent().addClass("athuz").fadeOut(500);
        //this a pont előtti részre hivatkozik, az adott objektumra
        //$("i").parent().addClass("athuz").fadeOut(500); az i-vel megegyező szinten lévő összes osztályt kiválasztja
    })

    //Összes feladat törlése
    $("#delete").on("click",function(){
        $("ol").html("");
    })

})