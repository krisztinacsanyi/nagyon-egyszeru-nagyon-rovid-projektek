
const text = document.querySelector(".text");


const textStr = text.textContent;


const textSplit = textStr.split("");

text.textContent="";


for(let i=0; i<textSplit.length; i++){
    text.innerHTML += "<span>" + textSplit[i] + "</span>";
}

let char= 0;
let timer = setInterval(onTime,50);

function onTime(){
    
    const span = document.querySelectorAll("span")[char];
    
    span.classList.add("fade");
    
    char++;
    
    if(char==textSplit.length){
       
        finish();
        return;
    }
}
function finish(){
  
    clearInterval(timer);
   
    timer = null;
}