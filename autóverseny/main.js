$(function(){
    $("#go").on("click",function(){

        function checkifComplete(){
            if(isComplete==false){
                isComplete=true;
            }
            else{
                place="Második";
            }
        }
        //versenyautók szélességének eltárolása változóban
        var carWidth = $(".car").width();

        //Versenypálya szélességének eltárolása változóban - kocsik szélessége, 
        //így a verseny végét azt fogja jelenteni ha a kocsi eleje éri el a célvonalat nem a kocsi hátulja
        var raceTrack = $(window).width() - carWidth;

        //adunk az autónak egy random sebességet: random generált szám 1 és 5000ms között
        var raceTime1=Math.floor( (Math.random()*5000)+1);
        var raceTime2=Math.floor( (Math.random()*5000)+1);

        //Alapállapot felvétele, hogy véget ért-e a verseny
        var isComplete=false;

        //Alapállapot felvétele, hogy ki ért be elsőnek
        var place="Első";

        //Első autó animálása
        $("#car1").animate({
            left: raceTrack
        },raceTime1, function(){
            checkifComplete();
            $("#raceInfo1 span").text(place + " helyen végzett " + raceTime1 + "ms alatt!");
        })

        //Második autó animálása
        $("#car2").animate({
            left: raceTrack
        },raceTime2, function(){
            checkifComplete();
            $("#raceInfo2 span").text(place + " helyen végzett " + raceTime2 + "ms alatt!");
        })
        //Újraindítás esetén mi történjen
        $("#reset").on("click",function(){
            $(".car").css("left",0);
            $(".raceInfo span").text("");
        })

    })
})